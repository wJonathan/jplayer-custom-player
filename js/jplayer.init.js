//<![CDATA[
jQuery(document).ready(function(){
	if(jQuery(window).width() > 600){
		//Set some Global Variables
		var playlist = [];
		var songs;
		var volume;
		var shown = false;
		
		//Get the songs from the database using Ajax
	    function get_songs(){ 
	    	var data = jQuery.ajax({
				url: jplayer_url.template_url+"/music-player/getsong.php",
				async: false
			}).responseText;
	
			songs = jQuery.parseJSON(data);
			shuffle(songs);
			for( var i = 0; i < songs.length; i++){
				myPlaylist.add( {title: songs[i][1], oga: songs[i][2], mp3: songs[i][0]});
			}
	    }
	    
	    //Shuffle the array to randomize the songs
	    function shuffle(array) {
		    return array.sort(function(){ 
			    return .5 - Math.random(); 
			});
		}
		

		// Create the jPlayer object and set the settings
		var myPlaylist = new jPlayerPlaylist({
			jPlayer: "#jquery_jplayer_1",
			cssSelectorAncestor: "#jp_container_1"
		}, playlist, {
			playlistOptions: {
				loopOnPrevious: true,
		
			},
			swfPath: "../js",
			loop: true,
			supplied: "mp3, oga",
			wmode: "window",
			smoothPlayBar: true,
			keyEnabled: true,
			verticalVolume: true,
			solution: "html, flash",
			
		});
		
		//Grab the Cookie if it exists and set Media or just set Media; Populate the Playlist
		if (sessionStorage.csID != null){
			//alert(sessionStorage.csID);
			var session_val = sessionStorage.csID.split('| ');
			var time = Math.floor(session_val[3]);
			var volume = Math.round(session_val[4] *10) / 10;
			//alert(volume);
			get_songs();
			jQuery("#jquery_jplayer_1").jPlayer("setMedia", 
				{
					title: session_val[0],
					oga: session_val[1],
					mp3: session_val[2]
			}).jPlayer("volume", volume);
			if(session_val[5] == "false"){
				//alert('Play');
				jQuery("#jquery_jplayer_1").jPlayer("play", time);
			}else if(session_val[5] == "true"){
				//alert('Pause');
				jQuery("#jquery_jplayer_1").jPlayer("pause", time);
			}
		}else{
			get_songs();
			var randomValue = Math.floor((Math.random() * songs.length));
			//alert(randomValue);
			jQuery("#jquery_jplayer_1").jPlayer("setMedia", 
				{
					title: songs[randomValue][1],
					oga: songs[randomValue][2],
					mp3: songs[randomValue][0]
			});
			jQuery("#jquery_jplayer_1").jPlayer("play");
		}
		
		//Set the title for the Current Track
		jQuery("#jquery_jplayer_1").bind(jQuery.jPlayer.event.play, function(event){
			{
	  			jQuery('.jp-current-track').html(event.jPlayer.status.media.title);	
	  		}
		});
		
		jQuery("#jquery_jplayer_1").bind(jQuery.jPlayer.event.loadstart, function(event){
			{
	  			jQuery('.jp-current-track').html(event.jPlayer.status.media.title);	
	  		}
		});
		
		//Set Cookies If Playing on timeupdate or if you pause it or change the volume
		jQuery("#jquery_jplayer_1").bind(jQuery.jPlayer.event.volumechange, function(event){
			{
	  				sessionStorage.csID = event.jPlayer.status.media.title+'| '+event.jPlayer.status.media.oga+'| '+event.jPlayer.status.media.mp3+'| '+event.jPlayer.status.currentTime+'| '+event.jPlayer.options.volume+'| '+event.jPlayer.status.paused;
	  		}
		});
		
		jQuery("#jquery_jplayer_1").bind(jQuery.jPlayer.event.timeupdate, function(event){
			if(typeof(Storage)!=="undefined")
	  		{
	  				sessionStorage.csID = event.jPlayer.status.media.title+'| '+event.jPlayer.status.media.oga+'| '+event.jPlayer.status.media.mp3+'| '+event.jPlayer.status.currentTime+'| '+event.jPlayer.options.volume+'| '+event.jPlayer.status.paused;
	  		}	
		});
		
		jQuery("#jquery_jplayer_1").bind(jQuery.jPlayer.event.pause, function(event){
			
			if(typeof(Storage)!=="undefined")
	  		{
	  				sessionStorage.csID = event.jPlayer.status.media.title+'| '+event.jPlayer.status.media.oga+'| '+event.jPlayer.status.media.mp3+'| '+event.jPlayer.status.currentTime+'| '+event.jPlayer.options.volume+'| '+event.jPlayer.status.paused+'| '+' |';
	  		}
		});
		
		//Fade in or out jp-volume-container when you hover over Mute or Unmute
		jQuery(".jp-mute").mouseenter(function() {
			if(shown == false){
				jQuery(".jp-volume-container").fadeIn('fast', function() {
					shown = true;
				});
			}else{
				jQuery(".jp-volume-container").fadeOut('fast', function() {
					shown = false;
				});
			}
	    });
	    
	    	jQuery(".jp-unmute").mouseenter(function() {
			if(shown == false){
				jQuery(".jp-volume-container").fadeIn('fast', function() {
					shown = true;
				});
			}else{
				jQuery(".jp-volume-container").fadeOut('fast', function() {
					shown = false;
				});
			}
	    });
	    
	}else{};
});
//]]>
